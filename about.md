---
layout: page
title: О нас
comments: false
---
**Магистраль - Газета Автомобильно-дорожного института ГОУВПО "Донецкий национальный технический университет"** <br>  

Учредитель - АДИ ДонНТУ  <br> 
    
Главный редактор Семененко В.В.   <br>

Выходит с декабря 2006 г.<br>
<br>
За достоверность фактов и цифр отвечает автор публикации. Мнения авторов могут не совпадать с позицией редакции. <br>

