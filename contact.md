---
layout: page
title: Контакты
---
**Магистраль - Газета Автомобильно-дорожного института ГОУВПО "Донецкий национальный технический университет"**   
   
Учредитель - АДИ ДонНТУ   
Главный редактор Семененко В.В.   
За достоверность фактов и цифр отвечает автор публикации. Мнения авторов могут не совпадать с позицией редакции.   
   
**Контакты**   
   
Адрес редакции: 84646,   
г. Горловка, 46,   
ул. Кирова, 51, Центр ОС   
Тел: +38(0624)552026   
E-mail: ois@adudonntu.ru  
   
  **Обратная связь**
   
<form action="https://formspree.io/{{site.email}}" method="POST">
    
    <div class="form-group row">

        <div class="col-md-6">
            <input class="form-control" type="text" name="name" placeholder="Имя">
        </div>

        <div class="col-md-6">
            <input class="form-control" type="email" name="_replyto" placeholder="E-mail адрес">
        </div>

    </div>

    <textarea rows="8" class="form-control mb-3" name="message" placeholder="Сообщение"></textarea>
    
    <input class="btn btn-success" type="submit" value="Отправить">

</form>